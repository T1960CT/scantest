/*
  Testing to confirm no congestion on repeated scans, 
  and to try to find out why sequential tests fail by not seeing devices on 6th scan
*/

import 'package:flutter_test/flutter_test.dart';
import 'package:scantest/main.dart';
import 'package:integration_test/integration_test.dart';

const String testDeviceName = 'HC-Virtual1';
void repeat() => expect(find.text(testDeviceName), findsOneWidget);

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('Group 1', () {
    testWidgets(
        'Scan g1 1',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g1 2',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g1 3',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g1 4',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    // testWidgets(
    //     'Scan g1 5',
    //     (t) async => await t
    //         .pumpWidget(const MyApp())
    // .then((_) => t.pumpAndSettle().then((_) => repeat())));
    // testWidgets(
    //     'Scan g1 6',
    //     (t) async => await t
    //         .pumpWidget(const MyApp())
    //         .then((_) => t.pumpAndSettle().then((_) => repeat())));
  });

  group('Group 2', () {
    testWidgets(
        'Scan g2 1',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g2 2',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g2 3',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g2 4',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g2 5',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
    testWidgets(
        'Scan g2 6',
        (t) async => await t
            .pumpWidget(const MyApp())
            .then((_) => t.pumpAndSettle().then((_) => repeat())));
  });
}
